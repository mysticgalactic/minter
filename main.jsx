import React from 'react';
import ReactDOM from 'react-dom/client';
import logoUrl from './src/images/logo.png';

import {ThemeProvider} from "@mui/material";
import darkTheme from './src/darkTheme'


// Web3 Modal
import { Web3Modal } from '@web3modal/react'
import {WagmiConfig} from "wagmi";
import mysticContract from "./src/lib/mysticContract";

import PurchaseContainer from "./src/components/PurchaseContainer";
import NFTViewer from "./src/components/NFTViewer";

const initPage = async () => {

    const reactApp = document.createElement("div");
    document.body.appendChild(reactApp);

    const root = ReactDOM.createRoot(reactApp);
    console.log(`logo image ${logoUrl}`)

    mysticContract.ipfsGateway= await IPFSHostAvailable('http://freenas:8080')
    if (!mysticContract.ipfsGateway) mysticContract.ipfsGateway = await IPFSHostAvailable('http://w3s.link')
    if (!mysticContract.ipfsGateway) mysticContract.ipfsGateway = await IPFSHostAvailable('https://cloudflare-ipfs.com')
    if (!mysticContract.ipfsGateway) mysticContract.ipfsGateway = await IPFSHostAvailable('http://localhost:8080')

    console.warn(`Using ipfs gateway: ${mysticContract.ipfsGateway}`)

    root.render(
        <React.StrictMode>
            <WagmiConfig client={mysticContract.wagmiClient}>
                <ThemeProvider theme = {darkTheme} >
                    <PurchaseContainer mysticContract={mysticContract} />
                    <NFTViewer mysticContract={mysticContract}/>
                </ThemeProvider>
            </WagmiConfig>
            <Web3Modal
                projectId={mysticContract.w3projectId}
                themeMode='dark'
                ethereumClient={mysticContract.ethereumClient}
                themeVariables={{
                   '--w3m-accent-color': '#575FEB',
                    '--w3m-font-family': 'Arial Narrow, sans-serif',
                    // '--w3m-background-border-radius': "0px",
                   '--w3m-logo-image-url': logoUrl,
                    '--w3m-input-border-radius': "25%",
                    '--w3m-background-color': "#3B3B3B"
               }}
            />
        </React.StrictMode>
    );
}


document.addEventListener("DOMContentLoaded", () => {
    initPage()
        .then(() => {
            console.log(`Mystic Galactic Minter Loaded`)
        }).catch((error) => {
        console.error(`Mystic Galactic Admin Minter Failed: ${error}`)
    });
});

async function IPFSHostAvailable(ipfsHost) {
    try {
        const response = await fetch(`${ipfsHost}/ipfs/bafybeid4gwmvbza257a7rx52bheeplwlaogshu4rgse3eaudfkfm7tx2my/hi-gateway.txt`);
        if (!response.ok) {
            // HTTP status error
            console.log(`IPFS Host blocked at ${ipfsHost} ... HTTP status: ${response.status}`);
            return null;
        }
        return ipfsHost;
    } catch (e) {
        // Network error
        console.log(`IPFS Host blocked at ${ipfsHost} ... Network error: ${e.message}`);
        return null;
    }
}
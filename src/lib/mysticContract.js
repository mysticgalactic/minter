import {mainnet} from "wagmi/chains";
import {alchemyProvider} from "wagmi/providers/alchemy";
import {EthereumClient, w3mConnectors} from "@web3modal/ethereum";
import {configureChains, createClient } from 'wagmi'


const DEFAULT_CHAIN = mainnet
const w3projectId = '4d1674f9dc9eea5ea94d709e47e1a1fe'
import compiledContract from '../abis/MysticGalacticMission1.json'



const chains = [DEFAULT_CHAIN]

const contractConfigs = JSON.parse(process.env.CONTRACTS);

// Configure provider
const { provider } = configureChains(chains, [
    alchemyProvider({ apiKey: contractConfigs.ethereum?.alchemyAPIKey, priority: 1}),
]);

// Create client
const wagmiClient = createClient({
    autoConnect: false,
    connectors: w3mConnectors({ w3projectId, version: 1, chains }),
    provider
});

const ethereumClient = new EthereumClient(wagmiClient, chains);


function getContractAddress(chain, compiledContract) {
    return compiledContract.networks[chain.id].address
}

// Define contract
const mysticContract  = {
    // contractConfigs: contractConfigs,
    wagmiClient: wagmiClient,
    ethereumClient: ethereumClient,
    defaultChain: DEFAULT_CHAIN,
    w3projectId: w3projectId,
    contractAddress: {
        address: getContractAddress(DEFAULT_CHAIN, compiledContract),
        abi: compiledContract.abi
    },
    ipfsGateway: "",
    depositRole: contractConfigs.ethereum?.depositKey,
    reserveRole: contractConfigs.ethereum?.reserveKey
}

export default mysticContract;
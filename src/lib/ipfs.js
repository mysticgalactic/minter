export function ipfsToHTTP(uri, mysticContract) {
    const cidPath = uri.replace('ipfs://', '');
    return `${mysticContract.ipfsGateway}/ipfs/${cidPath}`;
}
import {ethers} from "ethers";

export function formatCurrency(amount) {
    return new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    }).format(amount);
}

export function toUSD(value) {
    return value / 10 ** 2;
}

// Convert wei to ethers using formatUnits
export function weiToEthers(wei, decimals) {
    return parseFloat(ethers.utils.formatEther(wei)).toFixed(decimals);
}

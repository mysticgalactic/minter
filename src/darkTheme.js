import {createTheme} from "@mui/material";

const mode = 'dark'

const darkTheme = createTheme({
    spacing: 10,
    palette: {
        mode: mode,
        primary: {
            main: "#575FEB",
        },
        secondary: {
            main: "#575FEB",
        },
    },
    typography: {
        fontFamily: 'Arial Narrow, sans-serif',
        fontSize: 16,
        subtitle1: {
            fontSize: '16px',
            fontStyle: 'italic',
        },
        subtitle2: {
            fontSize: '12px',
            fontStyle: 'italic',
        },
    },
    text: mode === 'dark' ? { primary: 'white', secondary: 'grey' } : { primary: 'black', secondary: 'white' }
});

export default darkTheme;
import React, {useEffect, useState} from 'react';
import Alert from '@mui/material/Alert';
import Zoom from '@mui/material/Zoom';

export function AlertBox({ messageSetter }) {
    const [message, setMessage] = useState(null);
    const [zoomIn, setZoomIn] = useState(true);

    useEffect(() => {
        messageSetter(setMessage);
    }, [messageSetter]);

    return (
        message && message.text && message.severity && (
            <Zoom in={zoomIn} style={{ transitionDelay: zoomIn ? '500ms' : '0ms' }}>
                <Alert variant="outlined" severity={message.severity} style={{backgroundColor: 'transparent', boxShadow: 'none', border: 'none'}}>
                    <span style={{ color: message.severity === 'error' ? 'red' : 'inherit' }}>
                        {message.text}
                    </span>
                </Alert>
            </Zoom>
        )
    );
}


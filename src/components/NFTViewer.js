import React, { useEffect, useState } from 'react';
import {useAccount, useContract, useContractEvent, useProvider} from "wagmi";
import { Card, Box, Typography } from '@mui/material';
import NFTCard from "./NFTCard";
import theme from '../darkTheme'
import {ipfsToHTTP} from "../lib/ipfs";


function NFTViewer ({mysticContract}) {
    const [tokens, setTokens] = useState([]);
    const {address, isConnected} = useAccount()


    const provider = useProvider();
    const contract = useContract({
        ...mysticContract.contractAddress,
        signerOrProvider: provider
    })

    useContractEvent({
        ...mysticContract.contractAddress,
        eventName: 'TokenMinted',
        listener(from, paymentReceived, tokenId, tokenURI ) {
            async function fetchMetadata(id, uri) {
                const metadata = await fetch(ipfsToHTTP(uri, mysticContract)).then(res => res.json());
                setTokens(prevTokens => [...prevTokens, {id: id, uri: uri, metadata: metadata}]);
            }

            console.log(`${tokenId} -> ${tokenURI}`)
            fetchMetadata(tokenId.toString(), tokenURI);
        }
    });


    useEffect(() => {
        const fetchTokens = async () => {
            // Filter the events by the address
            const filter = contract.filters.TokenMinted(address);

            // Get all the matching events
            const events = await contract.queryFilter(filter);

            // Process the events
            const fetchedTokens = await Promise.all(events.map(async (event) => {
                const id = event.args.tokenId.toString();
                const uri = event.args.tokenURI;
                const owner = (await contract.ownerOf(id))
                if ( owner.toString() === address.toString() ) {
                    const metadata = await fetch(ipfsToHTTP(uri, mysticContract)).then(res => res.json());
                    return {id: id, uri: uri, metadata: metadata};
                } else {
                    return null;
                }
            }));

            setTokens(fetchedTokens);
        };

        if (isConnected ) {
            fetchTokens();
        }

    }, [contract, isConnected]);


    return (
        tokens.length > 0 && isConnected && (
            <Box sx={{ color: theme.palette.text.primary }}>
                <Typography variant="subtitle1" color="textPrimary">
                    {tokens.length === 1 ? 'Deposit ' : `Deposits - ${tokens.length} Bottles`}
                </Typography>

                <Box sx={{
                    overflow: 'auto',
                    maxHeight: '300px',
                    scrollbarWidth: 'thin',
                    scrollbarColor: 'black transparent',
                    "&::-webkit-scrollbar": {
                        width: 5
                    },
                    "&::-webkit-scrollbar-track": {
                        backgroundColor: "black"
                    },
                    "&::-webkit-scrollbar-thumb": {
                        backgroundColor: "gray",
                        borderRadius: 2
                    }
                }}>
                    {[...tokens].reverse().map((token) => (
                        <Card key={token.id}>
                            <NFTCard token={token} mysticContract={mysticContract}/>
                        </Card>
                    ))}
                </Box>
            </Box>
        )
    );
}

export default NFTViewer;
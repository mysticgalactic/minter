import React, { useEffect, useRef} from 'react';
import { CardContent, Typography, Link, Tooltip} from '@mui/material';
import openSeaLogoURL from '../images/opensea-dark.png';
// import eyeURL from '../images/eye.png';
import QRCodeStyling from "qr-code-styling";
import {ipfsToHTTP} from "../lib/ipfs";


function NFTCard({ token, mysticContract}) {
    const imageURL = ipfsToHTTP(token.metadata.image, mysticContract);
    const pdfURL = ipfsToHTTP(token.metadata.agreement, mysticContract);
    const qrCodeContainer = useRef(null);

    useEffect(() => {
        if (qrCodeContainer.current ) {
            qrCodeContainer.current.innerHTML = ''; // clear the container
            const qrCode = new QRCodeStyling({
                width: 100,
                height: 100,
                type: "svg",
                // image: {eyeURL},

                backgroundOptions: {
                    color: 'black'
                },
                cornersSquareOptions: {
                    color: "#4848B2",
                    type: 'square'
                },
                dotsOptions: {
                    color: '#4848B2',
                    type: 'classy'
                },
                data: getMemberPage()
                // imageOptions: {
                //     imageSize: 0.3,
                //     crossOrigin: "anonymous",
                //     margin: 20
                // }
            });
            qrCode.append(qrCodeContainer.current);
        }
    }, [token.id]);


    function getMemberPage () {
        return `https://www.mysticgalactic.com/member/${mysticContract.contractAddress.address}/${token.id}`
    }

    function openSeaBaseURL(chain) {
        switch (chain.id) {
            case 80001:
                return 'https://testnets.opensea.io/assets/mumbai'
            case 1:
                return 'https://opensea.io/assets/ethereum'
            default:
                return 'https://testnets.opensea.io/assets/mumbai'
        }
    }

    function openSeaURL (tokenId) {
        const baseurl = openSeaBaseURL(mysticContract.defaultChain);
        return `${baseurl}/${mysticContract.contractAddress.address}/${tokenId}`
    }

    return (
        <CardContent style={{ backgroundColor: "black", position: 'relative', display: 'flex', alignItems: 'flex-start' }}>
            <Tooltip title="Click to view deposit agreement" arrow>
                <Link href={pdfURL} target="_blank" rel="noreferrer">
                    <img src={imageURL} alt="Bottle Deposit NFT" style={{ width: '120px', height: '134px', objectFit: 'cover', marginRight: '20px' }}/>
                </Link>
            </Tooltip>
            <div>
                <Typography variant="body2" color="textPrimary" sx={{ fontSize: 12 }}>
                    {token.metadata.name}
                </Typography>
                <Tooltip title="Click to go to your personal member page or use this qr code with your phone">
                    <Link href={getMemberPage()} target="_blank" rel="noreferrer" underline="none">
                        <div ref={qrCodeContainer} style =  {{ backgroundColor: 'black'}} />
                    </Link>
                </Tooltip>
            </div>
            <div style={{ position: 'absolute', top: 0, right: "10px" }}>
                <Tooltip title="View or transfer your purchase on OpenSea">
                    <Link href={openSeaURL(token.id)} target="_blank" rel="noreferrer" underline="none">
                        <img src={openSeaLogoURL} alt="OpenSea NFT" style={{ width: '110px', height: '38px' }}/>
                    </Link>
                </Tooltip>
            </div>
        </CardContent>
    );
}

export default NFTCard;
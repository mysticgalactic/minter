import React, {useEffect, useState, useRef, useCallback} from 'react';
import {Box, Button, Grid, Slider, Typography} from '@mui/material';
import MuiInput from '@mui/material/Input';
import {styled} from '@mui/material/styles';
import {AlertBox} from './AlertBox'
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';
import theme from '../darkTheme'

import bottleUrl from '../images/bottle.png';

import { debounce } from 'lodash';

import {Web3Button} from "@web3modal/react";
import {
    useAccount,
    useContract,
    useContractRead,
    useContractWrite,
    useWaitForTransaction,
    usePrepareContractWrite,
    useProvider,
    useFeeData,
} from 'wagmi'
import {ethers, BigNumber} from "ethers";
import {formatEther} from "ethers/lib/utils";

const Input = styled(MuiInput)`
  width: 42px;
`;

const emptySummary= {
    TotalDepositPriceUSD: 0,
    TransactionFeeETH: 0,
    TransactionFeeUSD: 0,
    TotalEstimateUSD: 0,
    TotalEstimateETH: 0,
}


const MAX_MINT_PER_DAY = 15;

const PurchaseContainer = ({mysticContract}) => {
    const {address, isConnected} = useAccount()
    const [quantity, setQuantity] = useState(1);
    const [mintIt, setMintIt] = useState(false);
    const [currencyRate, setCurrencyRate] = useState(0);
    const [maxDailySupply, setMaxDailySupply] = useState(0);
    const [purchaseDisabled, setPurchaseDisabled] = useState(true)
    const [summary, setSummary] = useState(emptySummary)
    const [mintSucceeded, setMintSucceeded] = useState(false);
    const setAlertMessageRef = useRef();
    const [pricePerDeposit, setPricePerDeposit] = useState(BigNumber.from(0))
    const [pricePerDepositETH, setPricePerDepositETH] = useState(BigNumber.from(0))

    const setAlertMessage = (messageSetter) => {
        setAlertMessageRef.current = messageSetter;
    };

    const alert = {
        error: (msg) => {
            msg && setAlertMessageRef.current && setAlertMessageRef.current({text: msg, severity: "error"});
        },
        warn: (msg) => {
            msg && setAlertMessageRef.current && setAlertMessageRef.current({text: msg, severity: "warning"});
        },
        info: (msg) => {
            msg && setAlertMessageRef.current && setAlertMessageRef.current({text: msg, severity: "info"});
        },
        clear: () => {
            setAlertMessageRef.current && setAlertMessageRef.current({text: null, severity: null});
        }
    };

    const provider = useProvider();
    const contract = useContract({
        ...mysticContract.contractAddress,
        signerOrProvider: provider
    })

    const {data: gasFee} = useFeeData()

    // Read the Contract
    // const {data: AdminRole} = useContractRead({
    //     ...mysticContract.contractAddress,
    //     functionName: 'DEFAULT_ADMIN_ROLE',
    //     staleTime: Infinity,
    // })
    // const {data: isAdmin} = useContractRead({
    //     ...mysticContract.contractAddress,
    //     functionName: 'hasRole',
    //     args: [AdminRole, address],
    //     staleTime: Infinity,
    //     enabled: !!(address)
    // })
    // const {data: isReserveMinter} = useContractRead({
    //     ...mysticContract.contractAddress,
    //     functionName: "hasRole",
    //     args: [mysticContract.reserveRole, address],
    //     staleTime: Infinity,
    //     enabled: !!(address)
    // })
    const {data: isDepositoryAccount} = useContractRead({
        ...mysticContract.contractAddress,
        functionName: 'hasRole',
        args: [mysticContract.depositRole, address],
        staleTime: Infinity,
        enabled: !!(address)
    })
    const {data: priceInUSD} = useContractRead({
        ...mysticContract.contractAddress,
        functionName: 'priceInUSD',
        staleTime: Infinity,
    })
    const {data: priceInETH,} = useContractRead({
        ...mysticContract.contractAddress,
        functionName: 'price',
        watch: true,
    })
    const {data: initialSupply} = useContractRead({
        ...mysticContract.contractAddress,
        functionName: 'initialSupply',
        staleTime: Infinity,
    })
    const {data: supplyPerDay} = useContractRead({
        ...mysticContract.contractAddress,
        functionName: 'supplyPerDay',
        watch: true,
    })
    const {data: supply} = useContractRead({
        ...mysticContract.contractAddress,
        functionName: 'supply',
        watch: true,
    })
    const {data: supplyMintedToday} = useContractRead({
        ...mysticContract.contractAddress,
        functionName: 'supplyMintedToday',
        watch: true,
    })
    const {data: isPaused} = useContractRead({
        ...mysticContract.contractAddress,
        functionName: 'paused',
        watch: true,
    })

    // Write the Contract

    const {config: mintConfig/*, error:prepareError, isError:hasMintPrepareError */} = usePrepareContractWrite({
        ...mysticContract.contractAddress,
        stateMutability: 'payable',
        inputs: [{internalType: 'uint32', name: 'bottles', type: 'uint32'}],
        functionName: 'mintNFT',
        args: [BigNumber.from((quantity < 1) ? 1 : quantity)],
        overrides: {
            value: pricePerDepositETH?.mul(BigNumber.from(quantity < 1 ? 1: quantity))
        },
        onError(error) {
            switch( error.code ) {
                case 'UNPREDICTABLE_GAS_LIMIT': {
                    const maxSupply = supplyPerDay - supplyMintedToday ;
                    // console.warn(`Daily Limit Reached only maxSupply now: ${maxSupply}`)
                    setLimitedMaxSupply(maxSupply)
                }
            }
            // if ( error.transaction ) {
            //     console.error(`tx value is:  ${error.transaction.value.toString()}`);
            // }
            // if ( error.reason ) {
            //     console.error(`Error preparing contract: ${error.reason}`);
            // }
        },
        onSuccess() {
            const maxSupply = supplyPerDay - supplyMintedToday ;
            setLimitedMaxSupply(maxSupply)
        },
        enable:mintIt
    })
    const {data: mintResult, write: mint} = useContractWrite(mintConfig)
    const { isLoading:isMinting, isRefetching:isRefetchingMint} = useWaitForTransaction({
        hash: mintResult?.hash,
        onSuccess() {
            setMintSucceeded(true);
            alert.info(`Thank you for your purchase!`);
        },
        onError(error) {
            alert.info(`Failed to purchase NFT: ${error.message}`)
        }
    })

    useEffect(() => {
        if (isConnected) {
            alert.clear();
            updateSummary();
        }

    }, [isConnected, quantity, currencyRate, gasFee]);


    useEffect(() => {
        if (isConnected) {
            setLimitedMaxSupply(supplyPerDay);
            if ( isDepositoryAccount ) {
                setPricePerDeposit(BigNumber.from(0))
                setPricePerDepositETH(BigNumber.from(0))
            } else {
                setPricePerDeposit(priceInUSD)
                setPricePerDepositETH(priceInETH)
            }
        } else {
            alert.clear();
        }

    }, [isConnected, isDepositoryAccount, priceInETH, priceInUSD]);

    useEffect(() => {
        if ( !isPaused ) {
            alert.clear();
        }
    }, [isPaused]);

    useEffect(() => {
        if (isConnected && mintSucceeded) {
            setLimitedMaxSupply(supplyPerDay - supplyMintedToday);
            const timeout = setTimeout(() => {
                setMintSucceeded(false);
                alert.clear();
            }, 5000);
            // Clear the timeout when the component is unmounted or when the effect is run again
            return () => clearTimeout(timeout);
        }
    }, [mintSucceeded, isConnected]);


    useEffect(() => {
        const _doupdate = async () => {
            if (isConnected) {
                await updateCurrencyRate()

                // Set timer to continue updating fees
                const interval = setInterval(async () => {
                    await updateCurrencyRate();
                }, 30000); // Update every 10 seconds
                // Cleanup function to clear the interval when the component is unmounted
                return () => clearInterval(interval);
            }
        }
        _doupdate();

    }, [isConnected]);

    function setLimitedMaxSupply ( value ) {
        if (Number.isNaN(value)) {
            console.error(`supply max supply is being set to NaN`);
            return;
        }
        // console.warn(`updating supply to ${value}`);
        setMaxDailySupply(value > MAX_MINT_PER_DAY? MAX_MINT_PER_DAY : value);
    }

    async function updateSummary() {
        if (isConnected && quantity > 0 && (isDepositoryAccount || pricePerDepositETH.gt(BigNumber.from(0)))) {
            try {
                let _estimate = BigNumber.from(0);
                try {
                    _estimate = await contract.estimateGas["mintNFT"](quantity, {
                        from: address,
                        value: BigNumber.from(quantity).mul(pricePerDepositETH)
                        // gasLimit: BigNumber.from(20000)
                    });
                    setPurchaseDisabled(false);
                } catch (e) {
                    setPurchaseDisabled(true);
                    switch ( e.code ) {
                        case 'INSUFFICIENT_FUNDS':
                            alert.error(`Insufficient Funds in your wallet to make this purchase`);
                            break;
                        default:
                            if (e.reason && e.reason === 'execution reverted: Pausable: paused'){
                                alert.warn("Purchasing currently paused ... please check back later or contact info@mysticgalactic.com")
                                return;
                            } else if (e.reason && e.reason === 'reverted: Funds sent is less than the price of token value') {
                                // const valueSentToEstimate = e.transaction.value.toString()
                                alert.error(`Insufficient Funds in your wallet to make this purchase`);
                            } else {
                                console.error(`can't get gas estimate: ${e.reason} value: ${formatEther(valueSentToEstimate, 5)}`)
                                alert.info(`Delay in estimating gas ... please wait`);
                            }
                    }

                }

                const _estimateWithBuffer = _estimate.add(BigNumber.from(3000));

                try {
                    const chainId = provider._network.chainId
                    const transactionFeeInETH = _estimateWithBuffer.mul(gasFee.gasPrice)
                    const transactionFeeInUSD = await convertToUSD(chainId, transactionFeeInETH);
                    // const feeInNative = parseFloat(ethers.utils.formatEther(transactionFeeInETH));
                    const newSummary = {...summary}
                    newSummary.TransactionFeeUSD = transactionFeeInUSD;
                    newSummary.TransactionFeeETH = transactionFeeInETH;
                    const priceInUSD =  toUSD(parseFloat(pricePerDeposit.toString()));
                    newSummary.TotalDepositPriceUSD = (quantity * priceInUSD)
                    newSummary.TotalEstimateUSD = newSummary.TotalDepositPriceUSD +  transactionFeeInUSD;
                    newSummary.TotalEstimateETH = pricePerDepositETH.mul(BigNumber.from(quantity)).add(transactionFeeInETH);
                    setSummary(newSummary)
                    // console.log(`Update mint fees to gas ${_estimateWithBuffer} cost ${ transactionFeeInUSD}`);

                    if ( isPaused ) {
                        setPurchaseDisabled(true);
                        alert.warn("Purchasing is currently paused.  Please check back later.");
                    }

                } catch (e) {
                    console.warn(`warning: Failed to gas fee estimate: will try later: ${e.message}`);
                }
            } catch (error) {
                console.error('Failed to estimate gas for mintNFT:', error);
            }
        }
    }

    async function updateCurrencyRate() {
        const chainId = provider._network.chainId
        const chainIdToCurrencyId = {
            1: "ethereum", // Ethereum Mainnet
            5: "ethereum", // Goerli
            137: "matic-network", // Polygon Mainnet
            80001: "matic-network", // Polygon Mumbai
        };

        const currencyId = chainIdToCurrencyId[chainId];
        if (!currencyId) {
            throw new Error(`Unsupported chain ID: ${chainId}`);
        }

        try {
            const response = await fetch(`https://api.coingecko.com/api/v3/simple/price?ids=${currencyId}&vs_currencies=usd`);
            const data = await response.json();
            const usdRate = data[currencyId].usd;
            setCurrencyRate(usdRate)
            // console.log(`updated usd currency rate to ${usdRate}`)

        } catch (e) {
            console.warn("warning: failed to fetch currency")
        }
    }

    async function convertToUSD(chainId, value) {
        return parseFloat(ethers.utils.formatEther(value)) * currencyRate;
    }

    function formatCurrency(amount) {
        return new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        }).format(amount);
    }

    function toUSD(value) {
        return value / 10 ** 2;
    }

    function connectButton() {
        return (
            <Web3Button icon='show' label='Connect' balance='show'/>
        );
    }

    const handleBlur = () => {
        if (quantity < 1) {
            setQuantity(1);
        } else if (quantity > maxDailySupply) {
            setQuantity(maxDailySupply);
        }
    };

    function currencyLineItem(name, value, info) {
        return (
            <Grid item xs={12}>
                <Grid container justifyContent="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="subtitle1" color={isConnected ? 'text.primary' : 'text.disabled'}>
                            {name}
                            {info &&
                                <Tooltip title={info}>
                                    <IconButton aria-label="info" style={{ verticalAlign: 'super', fontSize: '0.8rem' }} >
                                        <InfoIcon fontSize="small" style={{ fontSize: '12px' }}/>
                                    </IconButton>
                                </Tooltip>
                            }
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography color={isConnected ? 'text.primary' : 'text.disabled'}>
                            {(value < 0.01 && value > 0.0) ?
                                <span style={{ fontSize: '0.875rem', fontStyle: 'italic' }}>
                                less than&nbsp;
                            </span>
                                : ""}
                            {isConnected ? formatCurrency((value < 0.01 && value > 0.0) ? 0.01 : value) : '---'}
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
        );
    }

    function line() {
        if (isConnected) {
            return <Grid item xs={12}>
                <Grid container>
                    <Grid item xs={6}></Grid>
                    <Grid item xs={6} container justifyContent="flex-end">
                        <Box
                            sx={(theme) => ({
                                width: "50%",
                                borderBottom: `1px solid ${theme.palette.text.primary}`,
                            })}
                        />
                    </Grid>
                </Grid>
            </Grid>;
        } else {
            return <Grid item xs={12}>
                <Grid container>
                    <Grid item xs={6}></Grid>
                    <Grid item xs={6} container justifyContent="flex-end"/>
                </Grid>
            </Grid>;

        }
    }

    // Convert wei to ethers using formatUnits
    function weiToEthers(wei, decimals) {
        return parseFloat(ethers.utils.formatEther(wei)).toFixed(decimals);
    }

    // Create a debounced function that delays invoking updateSummary until after wait milliseconds have elapsed since the last time the debounced function was invoked.
    const debouncedSetQuantity = useCallback(debounce(setQuantity, 150), [setQuantity]);
    const handleSliderChange = (event, newValue) => {
        debouncedSetQuantity(Number(newValue));
    };

    const handleInputChange = (event) => {
        const _quantity = event.target.value === '' ? 1 : Number(event.target.value);
        setQuantity(_quantity > 0 ? _quantity : 1);
    }

    const handlePurchaseClick = () => {
        setMintIt(true);
        mint?.()
    };

    function connectOrPurchase() {
        if (isConnected) {
            return (
                <Grid item xs={12} container justifyContent="center">
                    <Button
                        variant="contained"
                        color="primary"
                        size="small"
                        disabled={purchaseDisabled}
                        sx={{
                            backgroundColor: isConnected ? theme.palette.primary.main : theme.palette.warning.main,
                            fontSize: "16px",
                            width: "190px",
                            height: "25px",
                            borderRadius: "24px",
                            textTransform: "none"
                        }}
                        onClick={handlePurchaseClick}
                    >
                        {isMinting  && !isRefetchingMint && "Minting ..."}
                        {!isMinting && `Purchase ${weiToEthers(summary.TotalEstimateETH, 3)} ETH`}
                        {isRefetchingMint && "Trying Mint again ..."}
                    </Button>
                </Grid>
            );
        } else {
            return (
                <Grid item xs={12} container justifyContent="center"/>
            );
        }
    }

    function emptyLine(height) {
        return (
            <Grid item xs={12} container justifyContent="flex-end">
                <Box sx={{ width: "50%", height: `${height}` }} />
            </Grid>
        );
    }

    function quantitySelector() {
        if (isConnected) {
            return (
                <Grid container justifyContent="space-between">
                    <Box>
                        <Typography variant="subtitle1" color="text.primary">
                            Quantity
                        </Typography>
                        <Slider
                            value={quantity}
                            size="small"
                            onChange={handleSliderChange}
                            aria-labelledby="input-slider"
                            max={maxDailySupply}
                            min={1}
                            sx={{
                                color: "#575FEB",
                                width: '200px',
                            }}
                        />
                    </Box>
                    <Input
                        value={quantity}
                        // size="small"
                        onChange={handleInputChange}
                        onBlur={handleBlur}
                        inputProps={{
                            step: 1,
                            min: 1,
                            max: maxDailySupply,
                            color: "#575FEB",
                            type: 'number',
                            'aria-labelledby': 'input-slider',
                        }}
                        sx={{
                            color: theme.palette.text.primary, // Set the input text color
                            borderColor: theme.palette.text.primary, // Set the input border color
                            "&:hover": {
                                borderColor: theme.palette.text.primary, // Set the input border color on hover
                            },
                            "&:before": {
                                borderColor: "transparent !important", // Set the input underline color to transparent
                            },
                            "&:after": {
                                borderColor: theme.palette.text.primary, // Set the input underline color on focus
                            },
                            "& input": {
                                // Apply styles to the input element itself
                                textAlign: "right", // Align the text to the right
                                appearance: "textfield", // Hide the spinner for all browsers
                                "&::-webkit-inner-spin-button": {
                                    // Hide the spinner for WebKit browsers
                                    display: "none",
                                },
                            },
                        }}
                    />
                </Grid>
            );
        } else {
            return (
                <Grid container justifyContent="space-between"/>
            );
        }
    }

    function connectionLineItem() {
        if (isConnected) {
            return (
                <Grid container>
                    <Grid item xs={6} justifyContent="flex-end">
                        {/*<Typography variant="subtitle2" color={isConnected ? 'text.primary' : 'text.disabled'}>*/}
                        {/*    {supply}/{initialSupply}*/}
                        {/*</Typography>*/}
                        <div style={{display: 'flex', alignItems: 'center'}}>
                            <img src={bottleUrl} alt="Bottle Image" style={{marginRight: '8px', height: '30px', width: "30px"}}/>
                            <Typography variant="subtitle2" color={isConnected ? 'text.primary' : 'text.disabled'}>
                                {supply}/{initialSupply} Remaining
                            </Typography>
                        </div>
                    </Grid>
                    <Grid item xs={6} container justifyContent="flex-end">
                        {connectButton()}
                    </Grid>
                </Grid>
            );
        } else {
            return (
                <Grid container>
                    <Grid item xs={6} justifyContent="flex-end">
                    </Grid>
                    <Grid item xs={6} container justifyContent="flex-end">
                        {connectButton()}
                    </Grid>
                </Grid>
            );

        }
    }

    return (
        <Grid container spacing={0}>
            {connectionLineItem()}
            {quantitySelector(isConnected)}
            {currencyLineItem('Deposit Price', toUSD(pricePerDeposit))}
            {line(isConnected)}
            {currencyLineItem(
                'Total Deposit Price',
                summary.TotalDepositPriceUSD,
                `Total Price: This reflects the total cost to purchase ${quantity} bottles. The funds will be converted to USD and secured in a depository account. Please note, in the event the mission fails to secure sufficient deposits, or if the mission fails, these funds will be refunded to the current owner of the NFTs that were intended for purchase. All NFT deposits and bottle purchases are subject to the terms of the Bottle Deposit Agreement`
            )}
            {emptyLine('20px')}
            {currencyLineItem(
                'Est. Transaction Fee (gas)',
                summary.TransactionFeeUSD,
                "Estimated Transaction Fee: This is the estimated fee for processing your transaction on the blockchain, commonly known as a 'Gas fee'. It compensates miners for the computational resources required to validate the transaction. Please note, this fee does not go to the seller but is a necessary part of conducting transactions on the blockchain."
            )}
            {line(isConnected)}
            {currencyLineItem('Estimated Total Cost', summary.TotalEstimateUSD)}
            {emptyLine("20px")}
            {!mintSucceeded ? connectOrPurchase() : <div></div>}
            {emptyLine('20px')}
            <Grid container item xs={12} justifyContent="center">
                <AlertBox messageSetter={setAlertMessage} />
            </Grid>
            {emptyLine('40px')}
        </Grid>
    );
};

export default PurchaseContainer;
